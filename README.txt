
### Flash Block Readme

flashblock.module generates a flash animation using Ming
The end result is a Flash text scroller


Installation:
  Installation is like all normal modules (e.g. extract in the directory sites/all/modules)
  Please note that you also need to load colorpicker.module

Configuration:
  The configuration page is in your navigation menu and is called "Flash Text"
